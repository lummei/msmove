msmove
======

A modified version of Hudson's ms allowing for finer control and tracking of migrant genealogies.

To build msmove
```make release```

To build msiso
```gcc -O2 -Wall -o msiso msiso.c -lm```
